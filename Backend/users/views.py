from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.views import APIView
from .models import CustomUser
from .serializers import UserSerializer, GroupSerializer
from django.shortcuts import get_object_or_404

from rest_framework.response import Response

class UserListView(ListAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAdminUser]

class UserGroup(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user = self.request.user
        serializer_context = { 'request': request }
        serializer = GroupSerializer(user, many=False, context=serializer_context)
        return Response(serializer.data)