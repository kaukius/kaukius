Django>=3.0, <4.0
djangorestframework>=3.13.1
django-rest-auth>=0.9.5
django-allauth>=0.50.0
django-cors-headers>=3.11.0
mysqlclient>=2.1.0