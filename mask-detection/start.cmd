@echo off

if not exist venv\ (
	echo Creating virtual environment
	call python -m venv venv
	if %errorlevel% neq 0 exit /b %errorlevel%
)

call venv\Scripts\activate
if %errorlevel% neq 0 exit /b %errorlevel%
if defined VIRTUAL_ENV (
	call python start.py 0
) else (
	echo Couldn't activate virtual environment
	exit /b 2
)
cmd /k