import sys
import os
import subprocess
import pkg_resources

if __name__ == "__main__":
    # Protection from running outside of venv
    if len(sys.argv) != 2 or sys.argv[1].isnumeric() == False:
        print("Run this script using start.cmd")
        sys.exit(1)
    
    #installs and imports torch
    try:
        import torch
    except ModuleNotFoundError:
        print("Installing torch")
        subprocess.check_call("pip install torch==1.11.0+cu113 torchvision==0.12.0+cu113 torchaudio===0.11.0+cu113 -f https://download.pytorch.org/whl/cu113/torch_stable.html")
    
    # Installs and imports opencv
    try:
        import cv2
    except ModuleNotFoundError:
        print("Installing opencv")
        subprocess.check_call("pip install ipywidgets")
        subprocess.check_call("pip install opencv-python --upgrade")
    
    if not os.path.isdir("yolov5"):
        print("Installing yolov5")
        subprocess.check_call("git clone https://github.com/ultralytics/yolov5")
        subprocess.check_call("pip install -r ./yolov5/requirements.txt")
    
    # Donwloads weights if not exist
    if not os.path.exists("weights/best.pt"):
        weights_link = "https://we.tl/t-oedfKVsBAU"
        
        # Installs transferwee if doesn't exist
        if (not os.path.isdir("transferwee")):
            print("Installing transferwee")
            subprocess.check_call("git clone https://github.com/iamleot/transferwee")
            
         
        # Donwloads weights
        print("Downloading weights")
        os.mkdir("weights")
        subprocess.check_call(f"{sys.executable} transferwee/transferwee.py download {weights_link} -o \"weights/best.pt\"")
    
    import numpy as np
    
    #res640 = lambda img: A.Compose([A.LongestMaxSize(max_size=640, always_apply=True)])(image = img)["image"]
    model = torch.hub.load("ultralytics/yolov5", "custom", path="weights/best.pt")
        
        
    # Opens cam window
    cam = cv2.VideoCapture(int(sys.argv[1]))
    if (not cam.isOpened()):
        print("Couldn't connect to camera")
    
    while cam.isOpened():
        _, img = cam.read()
        detection = model(img)
        
        cv2.imshow("Detection", np.squeeze(detection.render()))
        if cv2.waitKey(10) == ord('q'):
            break
    cam.release()
    cv2.destroyAllWindows()