import styled  from 'styled-components'

export const Body = styled.div`
  display: flex;
  align-self: center;
  align-items: center;
  flex-direction:column;
  width: 30%;
  height: 200px;
  margin: 10% 35%;
  justify-content: space-between;
  @media screen and (max-width: 1000px) {
    flex-direction: column;
  }
`;
export const Header = styled.h1`
margin-left:0;
text-align: left;
width:70%;
`;