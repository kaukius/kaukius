import React, { useState, useEffect } from 'react';
import './login.css';
import ipaddress from '../../GLOBALIPADDRESS';




const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (localStorage.getItem('token') !== null) {
      window.location.replace('http://localhost:3000/client');
    } else {
      setLoading(false);
    }
  }, []);

  const onSubmit = e => {
    e.preventDefault();

    const user = {
      email: email,
      password: password
    };

    fetch(ipaddress+'/api/v1/users/auth/login/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
    })
      .then(res => res.json())
      .then(data => {
        if (data.key ) {
          console.log(data)
          localStorage.clear();
          localStorage.setItem('token', data.key);
          fetch(ipaddress+'/api/v1/users/auth/user/info/', {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Token ${localStorage.getItem('token')}`
            }
          })
            .then(res => res.json())
            .then(data => {

                localStorage.setItem('is_staff', data.is_staff);
                if(data.is_staff){
                  console.log("i was here")
                  window.location.replace('http://localhost:3000/dashboard');
                }
                else{
                  window.location.replace('http://localhost:3000/client');
                }
              });
        } else {
          setEmail('');
          setPassword('');
          localStorage.clear();
          setErrors(true);
        }
      });
  };

  return (
    <div className='div-login'>
      {loading === false && 
        <div className='div-login-logo'>
          <h1>Log in</h1>
        </div>
      }
      {errors === true && <h2>Cannot log in with provided credentials</h2>}
      {loading === false && (
        <form onSubmit={onSubmit}>
          <label htmlFor='email'>Email address:</label> <br />
          <input
            name='email'
            type='email'
            value={email}
            placeholder='email...'
            required
            onChange={e => setEmail(e.target.value)}
          />{' '}
          <br />
          <label htmlFor='password'>Password:</label> <br />
          <input
            name='password'
            type='password'
            value={password}
            placeholder='password...'
            required
            onChange={e => setPassword(e.target.value)}
          />{' '}
          <br />
          <input className='input-button' type='submit' value='Login' />
        </form>
      )}
    </div>
  );
};

export default Login;
