import React, { useState, useEffect, Fragment } from 'react';
import ipaddress from '../../GLOBALIPADDRESS';
import { Body } from './LogoutElements'
const Logout = () => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (localStorage.getItem('token') == null) {
      window.location.replace('http://localhost:3000/');
    } else {
      setLoading(false);
    }
  }, []);

  const handleLogout = e => {
    e.preventDefault();

    fetch(ipaddress + '/api/v1/users/auth/logout/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        localStorage.clear();
        window.location.replace('http://localhost:3000/');
      });
  };

  return (
    <div>
      {loading === false && (
          <Body>
            <h1>Are you sure you want to logout?</h1>
            <input className='input-button' type='button' value='Logout' onClick={handleLogout} />
          </Body>

      )}
    </div>
  );
};

export default Logout;
