import React, { useEffect, useState } from 'react'
import { Body, Text } from './ClientElements'
import ipaddress from '../../../GLOBALIPADDRESS';
const Client = () => {
    useEffect(() => {
        if (localStorage.getItem('token') === null ) {
          window.location.replace('http://localhost:3000/');
        } else if(localStorage.getItem('is_staff') == 'true') {
            window.location.replace('http://localhost:3000/dashboard');
        }
      }, []);
    const [canComeIn, setcanComeIn] = useState(false);
    const [minutes, setMinutes] = useState(2);
    const [seconds, setSeconds] = useState(0);
    useEffect(() => {
        let myInterval = setInterval(() => {
            if (seconds > 0) {
                setSeconds(seconds - 1);
            }
            if (seconds === 0) {
                if (minutes === 0) {
                    clearInterval(myInterval)
                } else {
                    setMinutes(minutes - 1);
                    setSeconds(59);
                }
            }
        }, 1000)
        return () => {
            clearInterval(myInterval);
        };
    });
    if(canComeIn){
            return (
        <Body>
            <Text>
                Come in!
            </Text>
        </Body>
    )
    }
    else{
        return (
            <Body>
                <Text>
                    Can come in 
                    <br/>
                    {seconds < 10 ?minutes + " : " + "0" + seconds :minutes + " : " + seconds}
                </Text>
            </Body>
        )
    }

}

export default Client