import styled  from 'styled-components'

export const Video = styled.video`
  width: 100%;
  margin: 5px;
  border-radius: 4px;
  @media screen and (max-width: 1600px) {
    width: 80%;
    margin: 5px;
    border-radius: 4px;
  }
`;

export const Body = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media screen and (max-width: 1000px) {
    flex-direction: column;
  }
`;

export const Header = styled.th`
text-align: left;
width:70%;
`;

export const Row = styled.tr`
border-radius: 4px;
display: flex;
justify-content: space-between;
align-items: center;
padding: 5% 5%;
width: 100%;


&:nth-child(odd){
    background: dodgerblue;
    color: #fff;
  }
`;
export const Table = styled.table`
display: flex;
flex-direction: column;
`;

export const Parameters = styled.div`
    width: 30%;
    margin: 0px 1%;
`;
export const Title = styled.h1`
    margin: 5% 0px;
`;

export const PeopleCounter = styled.button`
    margin-top: 30px;
    margin-left:25%;
    width: 50%;
    cursor: pointer;
    &:hover {
        transition: all 0.2s ease-in-out;
        background: #B5DAFF;
        color: #fff;
      }
`;