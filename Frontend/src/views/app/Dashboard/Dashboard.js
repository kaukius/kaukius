import React, { useState, useEffect, Fragment, useRef } from 'react';
import ipaddress from '../../../GLOBALIPADDRESS';
import {Video, Body, Header, Row, Table, Parameters, Title, PeopleCounter} from './DashboardElements';

const Dashboard = () => {
  useEffect(() => {
    if (localStorage.getItem('token') === null ) {
      window.location.replace('http://localhost:3000/');
    } else if (localStorage.getItem('is_staff') == 'false') {
      window.location.replace('http://localhost:3000/client');
    }
  }, []);
  const [peopleCount, setPeopleCount] = useState(0);
  const [error, setError] = useState("")
  const [maxCount, setMaxCount] = useState(24);
  const [ minutes, setMinutes ] = useState(0);
  const [seconds, setSeconds ] =  useState(0);
  const videoRef= useRef(null)
  const getVideo =() => {
    navigator.mediaDevices
      .getUserMedia({
        video: { width: 1920, height: 1080 }
      })
      .then(stream =>{
        let video =videoRef.current;
        video.srcObject = stream;
        video.play();
      })
      .catch(err => {
        console.error(err)
      })
  }
  useEffect(()=>{
    getVideo();
  }, [videoRef])
  useEffect(()=>{
    let myInterval = setInterval(() => {
            if (seconds > 0) {
                setSeconds(seconds - 1);
            }
            if (seconds === 0) {
                if (minutes === 0) {
                    clearInterval(myInterval)
                } else {
                    setMinutes(minutes - 1);
                    setSeconds(59);
                }
            } 
        }, 1000)
        return ()=> {
            clearInterval(myInterval);
          };
    });

  const PersonCameIn = () =>{
    if(peopleCount < maxCount){
      setError("")
      if(peopleCount+1 == maxCount){
        setMinutes(1)
      }
      setPeopleCount(peopleCount+1);
    }
    else(
      setError("Can't proceed this.")
    )
  }
  const PersonLeft = () =>{
    if(peopleCount > 0){
      setMinutes(0)
      setSeconds(0)
      setError("")
      setPeopleCount(peopleCount-1);
    }
    else(
      setError("Can't proceed this.")
    )
  }
  return (
    <Body>
    <Video ref={videoRef}></Video>
  <Parameters>
  <Title>Parameters</Title>
  <Table>
    <Row>
    <Header>Maximum people number</Header>
    <td>24</td>
    </Row>
    <Row>
      <Header>Current people number</Header>
      <td>{peopleCount}</td>
    </Row>
    <Row>
      <Header>Time left till a person can come in</Header>
      <td>{minutes} min {seconds} s</td>
    </Row>
  </Table>
  <PeopleCounter onClick={PersonLeft}>
    Person left
  </PeopleCounter>
  <PeopleCounter onClick={PersonCameIn}>
    Person came in
  </PeopleCounter>
  <p style={{color: 'red'}}>{error}</p>
  </Parameters>
</Body>
  );
};

export default Dashboard;
