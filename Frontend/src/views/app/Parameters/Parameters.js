import { render } from '@testing-library/react';
import React, { Fragment, useState, useEffect } from 'react'
import { Component } from 'react';
import "./parameters.css";
import data from "./data.json";
import ReadOnlyRow from './ReadOnlyRow';
import EditableRow from './EditableRow';
import ipaddress from '../../../GLOBALIPADDRESS';
const Parameters = () => {
  useEffect(() => {
    if (localStorage.getItem('token') === null ) {
      window.location.replace('http://localhost:3000/');
    } else if (localStorage.getItem('is_staff') == 'false') {
      console.log('I was here')
      window.location.replace('http://localhost:3000/client');
    }
  }, []);
  const [parameters, setParameters] = useState(data);  
  
  const [editFormData, setEditFormData] = useState({
    parameter: "",
    value: "",
    editable: "",
  })

  const [editParameterId, setEditParameterId] = useState(null);
  const handleEditClick = (event, parameter) => {
    event.preventDefault();
    setEditParameterId(parameter.id);

    const formValues = {
      parameter: parameter.parameter,
      value: parameter.value,
      editable: parameter.editable,
    }
    setEditFormData(formValues);
  };

  const handleEditFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...editFormData };
    newFormData[fieldName] = fieldValue;

    setEditFormData(newFormData)
  };

  const handleEditFormSubmit = (event) => {
    event.preventDefault();

    const editedParameter = {
      id: editParameterId,
      parameter: editFormData.parameter,
      value: editFormData.value,
      editable: editFormData.editable
    }


    const newParameters = [...parameters]

    const index = parameters.findIndex((parameter) => parameter.id === editParameterId)

    newParameters[index] = editedParameter;
    if (index == 0) {
      newParameters[1].value = Math.floor(editedParameter.value / 15);
    }

    if (index == 2 && editedParameter.value > parameters[1].value) {
      newParameters[2].value = newParameters[1].value;
    }

    setParameters(newParameters);


    setEditParameterId(null);

  }

  const handleCancelClick = () => {
    setEditParameterId(null);
  }

  return (
    <div className='parameters-container'>
      <form onSubmit={handleEditFormSubmit}>
        <table className='parameters-table'>
          <thead>
            <tr>
              <th>Parameter</th>
              <th>Value</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {parameters.map((parameter) =>
              <Fragment>
                {editParameterId === parameter.id && parameter.id !== 2 ? (<EditableRow parameter={parameter} editFormData=
                  {editFormData} handleEditFormChange={handleEditFormChange} handleCancelClick={handleCancelClick} />)
                  : (<ReadOnlyRow parameter={parameter} handleEditClick={handleEditClick} />)}
              </Fragment>
            )}
          </tbody>
        </table>
      </form>
    </div>);


};

export default Parameters
