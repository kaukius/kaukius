import React from "react";

const ReadOnlyRow = ({parameter, handleEditClick}) =>{
    console.log(parameter)
    return(
    <tr>
        <td>{parameter.parameter}</td>
        <td>{parameter.value}</td>      
        <td>
            {parameter.editable ? <button type="button" onClick={(event) => handleEditClick(event, parameter)}>Edit</button>: "⠀" }
            
        </td>
      </tr>
    )
}

export default ReadOnlyRow