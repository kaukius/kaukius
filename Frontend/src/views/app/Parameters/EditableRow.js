import React from "react";
import "./parameters.css";

const EditableOnlyRow = ({parameter, editFormData, handleEditFormChange, handleCancelClick}) =>{
    return(
    <tr>
        <td>{parameter.parameter}</td>
        <td><input type={"number"} required="required" placeholder="Enter a value" name="value" value={editFormData.value} onChange={handleEditFormChange}></input></td>            
        <td className="divider">
            <button className="item" type="submit">Save</button>
            <button type="button" onClick={handleCancelClick}>Cancel</button>
        </td>
        
      </tr>
    )
}

export default EditableOnlyRow