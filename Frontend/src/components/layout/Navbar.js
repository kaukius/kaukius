import React, { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Bars, Nav } from './NavbarElements';
import { NavLink } from './NavbarElements';
import { NavMenu } from './NavbarElements';
import { NavBtn } from './NavbarElements';
import { NavBtnLink } from './NavbarElements';
const Navbar = () => {
  const [isAuth, setIsAuth] = useState(false);

  useEffect(() => {
    if (localStorage.getItem('token') !== null) {
      setIsAuth(true);
    }
  }, []);

  if (localStorage.getItem('is_staff') == 'true') {
    return (
      <Nav>
        <NavLink to='/'>
          <h1>Logo</h1>
        </NavLink>
        <Bars />
        <NavMenu>
          <NavLink to='/parameters' activeStyle>
            Parameters
          </NavLink>
        </NavMenu>
        <NavBtn>
          <NavBtnLink to='/logout'>Log Out</NavBtnLink>
        </NavBtn>
      </Nav>
    );
  }
  else {
    return (
      <Nav>
        <NavLink to='/'>
          <h1>Logo</h1>
        </NavLink>
        <Bars />
        <NavBtn>
          <NavBtnLink to='/logout'>Log Out</NavBtnLink>
        </NavBtn>
      </Nav>
    )
  }
};

export default Navbar;
