import React from 'react'
import {FooterCSS} from './FooterElements';

function Footer() {
  return (
        <FooterCSS>© K173 - Kaukius</FooterCSS>
  )
}

export default Footer