import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import Login from './views/auth/Login';
import Signup from './views/auth/Signup';
import Logout from './views/auth/Logout';
import Dashboard from './views/app/Dashboard/Dashboard';
import './App.css';
import Footer from './components/layout/Footer';
import  Parameters  from './views/app/Parameters/Parameters';
import Client from './views/app/Client/Client';

const App = () => {
  return (
    <div style={{ display: 'flex', minHeight: '100vh', flexDirection: 'column', flex: '1' }}>
      <Router>
        <Switch>
          <Route path='/' component={Login} exact />
          <Route path='/parameters' component={ () => <>< Navbar /><Parameters /> <Footer/></>} exact />
          <Route path='/logout' component={ () => <>< Navbar /><Logout /> <Footer/></>} exact />
          <Route exact path='/dashboard' component={ () => <>< Navbar /><Dashboard /> <Footer/> </>} />
          <Route path='/client' component={Client} exact />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
